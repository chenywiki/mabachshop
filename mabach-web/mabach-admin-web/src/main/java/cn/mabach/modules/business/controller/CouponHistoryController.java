package cn.mabach.modules.business.controller;

import cn.mabach.business.entity.CouponHistoryEntity;
import cn.mabach.modules.business.feign.CouponHistoryServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 优惠券使用、领取历史表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 18:31:56
 */
@RestController
@RequestMapping("/couponHistory")
public class CouponHistoryController {
    @Autowired
    private CouponHistoryServiceFeign couponHistoryService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<CouponHistoryEntity> getList(@RequestParam(value = "couponId", required = false) Long couponId,
                                                   @RequestParam(value = "useStatus", required = false) Integer useStatus,
                                                   @RequestParam(value = "orderSn", required = false) String orderSn,
                                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<CouponHistoryEntity>> pageResultRS = couponHistoryService.queryPage(couponId,useStatus,orderSn, pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public CouponHistoryEntity getItem(@PathVariable("id") Long id) {
        return couponHistoryService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody CouponHistoryEntity couponHistoryEntity) {

        RS rs = couponHistoryService.saveE(couponHistoryEntity);

        return rs;
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody CouponHistoryEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = couponHistoryService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = couponHistoryService.removeByIdsE(ids);
        return rs;
    }





}
