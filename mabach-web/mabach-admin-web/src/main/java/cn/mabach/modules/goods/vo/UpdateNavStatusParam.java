package cn.mabach.modules.goods.vo;

import lombok.Data;

@Data
public class UpdateNavStatusParam {
    Long[] ids;
    Integer navStatus;
}
