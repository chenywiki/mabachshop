package cn.mabach.modules.business.feign;

import cn.mabach.business.service.HomeAdvertiseService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface HomeAdvertiseServiceFeign extends HomeAdvertiseService {
}
