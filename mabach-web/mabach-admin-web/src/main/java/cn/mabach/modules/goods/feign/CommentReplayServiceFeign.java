package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.CommentReplayService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface CommentReplayServiceFeign extends CommentReplayService {
}
