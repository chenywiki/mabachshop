package cn.mabach.modules.business.controller;

import cn.mabach.business.dto.FlashPromotionProduct;
import cn.mabach.business.entity.FlashPromotionProductRelationEntity;
import cn.mabach.modules.business.feign.FlashPromotionProductRelationServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品限时购与商品关系表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@RestController
@RequestMapping("/flashProductRelation")
public class FlashPromotionProductRelationController {
    @Autowired
    private FlashPromotionProductRelationServiceFeign flashPromotionProductRelationService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<FlashPromotionProduct> getList(@RequestParam(value = "flashPromotionId",required = false) Long flashPromotionId,
                                                     @RequestParam(value = "flashPromotionSessionId",required = false) Long flashPromotionSessionId,
                                                     @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                     @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<FlashPromotionProduct>> pageResultRS = flashPromotionProductRelationService.queryPage(flashPromotionId, flashPromotionSessionId,pageNum, pageSize);
        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public FlashPromotionProductRelationEntity getItem(@PathVariable("id") Long id) {
        return flashPromotionProductRelationService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody List<FlashPromotionProductRelationEntity> list) {

        RS rs = flashPromotionProductRelationService.saveS(list);

        return rs;
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody FlashPromotionProductRelationEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = flashPromotionProductRelationService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = flashPromotionProductRelationService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return flashPromotionProductRelationService.removeByIdE(id);

    }


}
