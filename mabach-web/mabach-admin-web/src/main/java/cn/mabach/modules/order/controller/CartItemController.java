package cn.mabach.modules.order.controller;


import cn.mabach.order.entity.CartItemEntity;
import cn.mabach.order.service.CartItemService;


import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 购物车表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@RestController
@RequestMapping("/cartitem")
public class CartItemController {
    @Autowired
    private CartItemService cartItemService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<CartItemEntity> getList(@RequestParam(value = "keyword", required = false) String keyword,
                                              @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                              @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<CartItemEntity>> pageResultRS = cartItemService.queryPage(keyword, pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public CartItemEntity getItem(@PathVariable("id") Long id) {
        return cartItemService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody CartItemEntity cartItemEntity) {

        RS rs = cartItemService.saveE(cartItemEntity);

        return rs;
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody CartItemEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = cartItemService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = cartItemService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return cartItemService.removeByIdE(id);

    }


}
