package cn.mabach.modules.business.feign;

import cn.mabach.business.service.FlashPromotionProductRelationService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface FlashPromotionProductRelationServiceFeign extends FlashPromotionProductRelationService {
}
