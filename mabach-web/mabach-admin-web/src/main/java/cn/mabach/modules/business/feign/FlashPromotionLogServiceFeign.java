package cn.mabach.modules.business.feign;

import cn.mabach.business.service.FlashPromotionLogService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface FlashPromotionLogServiceFeign extends FlashPromotionLogService {
}
