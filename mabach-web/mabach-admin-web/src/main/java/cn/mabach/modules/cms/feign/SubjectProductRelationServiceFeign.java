package cn.mabach.modules.cms.feign;

import cn.mabach.cms.service.SubjectProductRelationService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-cms")
public interface SubjectProductRelationServiceFeign extends SubjectProductRelationService {
}
