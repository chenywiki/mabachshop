package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.SkuStockService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface SkuStockServiceFeign extends SkuStockService {
}
