package cn.mabach.modules.EsLog;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.Map;

/**
 * @Author: ming
 * @Date: 2020/2/16 0016 下午 4:10
 */
@Data
@Document(indexName = "infolog",type = "info",shards = 3)
public class InfologEntity {


    private String type;

    private Map<String,String> tags;

    @Field(index = true,store = true,type = FieldType.Date)
    private String logdate;

    @Field(index = true,store = true,type = FieldType.Text,analyzer = "ik_smart")
    private String message;


}
