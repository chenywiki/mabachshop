package cn.mabach.modules.business.feign;

import cn.mabach.business.service.HomeRecommendSubjectService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface HomeRecommendSubjectServiceFeign extends HomeRecommendSubjectService {
}
