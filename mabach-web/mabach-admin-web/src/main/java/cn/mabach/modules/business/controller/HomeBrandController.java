package cn.mabach.modules.business.controller;

import cn.mabach.business.entity.HomeBrandEntity;
import cn.mabach.modules.business.feign.HomeBrandServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 首页推荐品牌表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@RestController
@RequestMapping("/home/brand")
public class HomeBrandController {
    @Autowired
    private HomeBrandServiceFeign homeBrandService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<HomeBrandEntity> getList(@RequestParam(value = "brandName", required = false) String brandName,
                                               @RequestParam(value = "recommendStatus", required = false) Integer recommendStatus,
                                               @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                               @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<HomeBrandEntity>> pageResultRS = homeBrandService.queryPage(brandName,recommendStatus, pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public HomeBrandEntity getItem(@PathVariable("id") Long id) {
        return homeBrandService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    @Transactional
    public RS create(@RequestBody List<HomeBrandEntity> selectBrands) {

        for (HomeBrandEntity homeBrandEntity : selectBrands) {
            RS rs = homeBrandService.saveE(homeBrandEntity);
            if (rs.getCode()!=200){
                return RS.error();
            }
        }


        return RS.ok();
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update")
    public RS update(@RequestParam("ids") Long ids,
                     @RequestParam("recommendStatus") Integer recommendStatus) {
        HomeBrandEntity entity = new HomeBrandEntity();
        entity.setId(ids);
        entity.setRecommendStatus(recommendStatus);
        RS rs = homeBrandService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = homeBrandService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return homeBrandService.removeByIdE(id);

    }

    /*
    * 修改品牌排序
    * */
    @RequestMapping(value = "/update/sort", method = RequestMethod.POST)
    public RS updateSort(@RequestParam("id") Long id, @RequestParam("sort") Integer sort) {
        return homeBrandService.updateSort(id,sort);

    }

    /*
    * 批量修改推荐状态
    * */
    @RequestMapping(value = "/update/recommendStatus", method = RequestMethod.POST)
    public RS updateRecommendStatus(@RequestParam("ids") List<Long> ids, @RequestParam Integer recommendStatus) {
       return homeBrandService.updateRecommendStatus(ids,recommendStatus);
    }
}
