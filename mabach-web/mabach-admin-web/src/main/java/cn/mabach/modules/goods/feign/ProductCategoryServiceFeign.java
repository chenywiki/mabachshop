package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.ProductCategoryService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface ProductCategoryServiceFeign extends ProductCategoryService {
}
