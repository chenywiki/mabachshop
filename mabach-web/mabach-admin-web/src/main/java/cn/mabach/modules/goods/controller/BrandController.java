package cn.mabach.modules.goods.controller;


import cn.mabach.modules.goods.feign.BrandServiceFeign;
import cn.mabach.goods.entity.BrandEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 品牌功能Controller
 * Created by macro on 2018/4/26.
 */

@RestController
@RequestMapping("/brand")
@Slf4j
public class BrandController {
    @Autowired
    private BrandServiceFeign brandService;
    ThreadLocal<String> threadLocal = new ThreadLocal<>();

    @RequestMapping(value = "/listAll", method = RequestMethod.GET)
    public List<BrandEntity> getList() {

        RS<List<BrandEntity>> listRS = brandService.queryAll();


        return listRS.getData();
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public PageResult<BrandEntity> getList(@RequestParam(value = "keyword", required = false) String keyword,
                                               @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                               @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize) {
        RS<PageResult<BrandEntity>> pageResultRS = brandService.queryPage(keyword, pageNum, pageSize);


        return pageResultRS.getData();
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public RS create(@Validated @RequestBody BrandEntity brandEntity, BindingResult result) {

        RS rs = brandService.saveE(brandEntity);


        return rs;
    }


    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody BrandEntity brandEntity,
                     BindingResult result) {
        brandEntity.setId(id);
        RS rs = brandService.updateByIdE(brandEntity);



        return rs;

    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public RS delete(@PathVariable("id") Long id) {
        RS rs = brandService.removeOneById(id);

        return rs;
    }





    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public BrandEntity getItem(@PathVariable("id") Long id) {
        return brandService.getByIdE(id).getData();
    }



    @RequestMapping(value = "/delete/batch", method = RequestMethod.POST)
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = brandService.removeByIdsE(ids);

        return rs;
    }



/*
*批量更新显示状态
* */
    @RequestMapping(value = "/update/showStatus", method = RequestMethod.POST)
    public RS updateShowStatus(@RequestParam("ids") List<Long> ids,
                               @RequestParam("showStatus") Integer showStatus) {

        RS rs = brandService.updateShowStatus(ids, showStatus);

        return rs;
    }


    /*
     * 批量更新厂家制造商状态
     * */
    @PostMapping(value = "/update/factoryStatus")
    public RS updateFactoryStatus(@RequestParam("ids") Long[] ids,
                                  @RequestParam("factoryStatus") Integer factoryStatus) {

        return brandService.updateFactoryStatus(Arrays.asList(ids),factoryStatus);
    }
}
