package cn.mabach.modules.business.feign;

import cn.mabach.business.service.FlashPromotionService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface FlashPromotionServiceFeign extends FlashPromotionService {
}
