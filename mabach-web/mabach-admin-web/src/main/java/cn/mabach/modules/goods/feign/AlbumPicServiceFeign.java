package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.AlbumPicService;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient(name = "app-mabach-goods")
public interface AlbumPicServiceFeign extends AlbumPicService {
}
