package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.ProductOperateLogService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface ProductOperateLogServiceFeign extends ProductOperateLogService {
}
