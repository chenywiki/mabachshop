

var g_orderby = null;
var g_categoryFilter = null;
var g_tagFilter = null;

window.onload = function() {

    var slider =
        Swipe(document.getElementById('slider'), {
            auto: 1000000,
            continuous: true,
            callback: function(pos) {
            }
        });
}

getFloat = function (number, n) {
    n = n ? parseInt(n) : 0;
    if (n <= 0) return Math.round(number);
    number = Math.round(number * Math.pow(10, n)) / Math.pow(10, n);
    return number;
};

jQuery(document).ready(function() {
    var g_currentUser = null;
    $("#searchTitle").hide();
    tuijian();

    var geolocation = new BMap.Geolocation();
    geolocation.getCurrentPosition(function(r){
        if(this.getStatus() == BMAP_STATUS_SUCCESS){
            var longitude = r.point.lng;
            var latitude = r.point.lat;
            $("#longitude").val(longitude);
            $("#latitude").val(latitude);

        }
        else {
            alert('failed'+this.getStatus());
        }
    },{enableHighAccuracy: true});



    //获取当前用户的信息
    $.ajax({
        type:"POST",
        contentType:"application/json;charset=utf-8",
        url:"/portal/getcurrentuser",
        success:function(data){
            if(data.flag){
                if(data.data == null){
                    g_currentUser = null;
                }else{
                    g_currentUser = data.data;
                }
                if(g_currentUser == null){
                    $("#rightTitle").text("登录");
                }else{
                    $("#rightTitle").text("注销");
                }
            }
        },

    });


    $("#toRecommend").on("click",function(){

        $("#guess").show();
        $("#tuanList").show();
        $("#searchTitle").hide();
        $("#searchList").hide();
        $("#slider").hide();
        tuijian()
    });

    $("#tuijian2").on("click",function(){
        $("#guess").show();
        $("#tuanList").show();
        $("#searchTitle").hide();
        $("#searchList").hide();
        $("#slider").hide();
        tuijian()
    });

    $("#category1").on("click",function(){
        searchByCategory("中餐")

    });

    $("#category2").on("click",function(){
        searchByCategory("饮品甜点")

    });
    $("#category3").on("click",function(){
        searchByCategory("快餐")

    });
    $("#category4").on("click",function(){
        searchByCategory("粉面")

    });
    $("#category5").on("click",function(){
        searchByCategory("烧烤")

    });
    $("#category6").on("click",function(){
        searchByCategory("外国菜")

    });
    $("#category7").on("click",function(){
        searchByCategory("小吃")

    });
    $("#category8").on("click",function(){
        searchByCategory("其他")

    });



    $("#rightTitle").on("click",function(){
        if(g_currentUser == null){
            window.location.href="/api-oauth/toLogin?from=http://www.mabach.cn/api-portal/";
            return false;
        }else{
            $.ajax({
                type:"POST",
                contentType:"application/json;charset=utf-8",
                url:"/api-oauth/logout",
                success:function(data){

                    window.location.href="/api-portal/";


                },

            });
        }
        return false;
    });

    function tuijian() {

        $.ajax({
            type : "GET",
            url : "/dianping/recommend",
            success : function(data){
                if(data.flag){
                    var dataList = data.data;
                    for(var i = 0; i < dataList.length; i++){

                        var content = $('<a target="_blank"  href="dianping/details?id='+dataList[i].id+'" class="item Fix hightitem">'+
                            '<div class="cnt">'+
                            '<img class="pic" src="'+dataList[i].logo+'"/>'+
                            '<div class="wrap">'+
                            '<div class="wrap2">'+
                            '<div class="content">'+
                            '<div class="shopname">'+dataList[i].name+'</div>'+
                            '<div class="title">店内热销： '+dataList[i].hot+'</div>'+
                            '<div class="info">'+
                            '<span class="symbol">¥</span>'+
                            '<span class="price">'+dataList[i].pricePerMan+'</span></div> </div> </div> </div> </div></a>');
                        $("#tuanList").append(content);
                    }
                }
            },

        });
    };

    function searchByCategory(keyword){

        var orderby = "";
        var categoryFilter = "";
        var tagFilter = "";

        var longitude = $("#longitude").val();
        var latitude = $("#latitude").val();

        if(longitude== "" || latitude == "" ){
            alert("未获取到地理位置，请重新刷新");
            return false;
        }
        if(g_orderby == null){
            orderby = "";
        }else{
            orderby = "&orderby="+g_orderby;
        }

        if(g_categoryFilter == null){
            categoryFilter = "";
        }else{
            categoryFilter = "&categoryId="+g_categoryFilter;
        }
        if(g_tagFilter == null){
            tagFilter = "";
        }else{
            tagFilter = "&tags="+g_tagFilter;
        }
        $.ajax({
            type : "POST",
            contentType : "application/json; charset=utf-8",
            url : "/dianping/search?keyword="+keyword+"&longitude="+longitude+"&latitude="+latitude+orderby+categoryFilter+tagFilter,
            success : function(data){
                if(data.rs.flag ){
                    $("#guess").hide();
                    $("#tuanList").hide();
                    $("#searchTitle").show();
                    $("#searchList").show();
                    $("#searchList").empty();
                    $("#slider").hide();
                    var dataList = data.rs.data;
                    var categoryList = data.category;

                    var tagsList = data.tags;
                    for(var i = 0; i < dataList.length; i++){
                        var distance = dataList[i].distance;
                        var distanceStr = "";
                        if(distance >= 1000){
                            distanceStr = getFloat(distance / 1000,1) + " km";
                        }else{
                            distanceStr = distance + " m";
                        }
                        var content = $('<a target="_blank"  href="dianping/details?id='+dataList[i].id+'" class="item Fix hightitem">'+
                            '<div class="cnt">'+
                            '<img class="pic" src="'+dataList[i].logo+'"/>'+
                            '<div class="wrap">'+
                            '<div class="wrap2">'+
                            '<div class="content">'+
                            '<div class="shopname">'+dataList[i].name+'</div><span >'+'</span>'+
                            '<div class="title"><span>店内热卖：</span>' +dataList[i].hot +'</div>'+

                            '<div class="info">'+
                            '<span class="symbol">¥</span>'+
                            '<span class="price">'+dataList[i].pricePerMan+'</span>&nbsp &nbsp &nbsp<span>评分</span>&nbsp<span class="price">'+dataList[i].remarkScore+'</span><span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp距离:'+distanceStr+'</span></div> </div> </div> </div> </div></a>');
                        $("#searchList").append(content);
                    }
                    $("#categoryFilter").empty();
                    var categoryTag = $("<span class='bubble' data-id='"+0+"' id='category"+0+"'>全部</span>");
                    $("#categoryFilter").append(categoryTag);
                    for(var i = 0; i < categoryList.length; i++){
                        var categoryTag = $("<span class='bubble' data-id='"+categoryList[i].id+"' id='category"+categoryList[i].id+"'>"+categoryList[i].name+"</span>");
                        $("#categoryFilter").append(categoryTag);
                    }


                    $("span[id^='category']").on('click',function(){
                        var id = $(this).data("id");
                        if(id == 0){
                            g_categoryFilter = null;
                        }else{
                            g_categoryFilter = id;
                        }
                        searchMethod();
                    });

                    $("#priceFilter").empty();
                    $("#priceFilter").append($('<span class="bubble" id="defaultOrder">默认排序</span>'));
                    $("#priceFilter").append($('<span class="bubble" id="lowPriceOrder">低价排序</span>'));

                    $("#defaultOrder").on('click',function(){
                        g_orderby=null;
                        searchMethod();
                    });
                    $("#lowPriceOrder").on('click',function(){
                        g_orderby=1;
                        searchMethod();
                    });

                    $("#tagsFilter").empty();
                    var tagTag = $("<span class='bubble' data-id='' id='tags'>全部</span>");
                    $("#tagsFilter").append(tagTag);
                    for(var i = 0; i < tagsList.length; i++){


                        var tagTag = $("<span class='bubble' data-id='"+tagsList[i].tags+"' id='tags"+tagsList[i].tags+"'>"+tagsList[i].tags+"("+tagsList[i].num+")</span>");
                        $("#tagsFilter").append(tagTag);


                    }
                    $("span[id^='tags']").on('click',function(){
                        var id = $(this).data("id");
                        if(id == ''){
                            g_tagFilter = null;
                        }else{
                            g_tagFilter = id;
                        }
                        searchMethod();
                    });
                }
            },

        });
    };




    function searchMethod(){

        var orderby = "";
        var categoryFilter = "";
        var tagFilter = "";

        var keyword = $("#keyword").val();
        var longitude = $("#longitude").val();
        var latitude = $("#latitude").val();
        if($("#keyword").val() == ""){
            alert("关键字不能为空");
            return false;
        }
        if(longitude== "" || latitude == "" ){
            alert("未获取到地理位置，请重新刷新");
            return false;
        }
        if(g_orderby == null){
            orderby = "";
        }else{
            orderby = "&orderby="+g_orderby;
        }

        if(g_categoryFilter == null){
            categoryFilter = "";
        }else{
            categoryFilter = "&categoryId="+g_categoryFilter;
        }
        if(g_tagFilter == null){
            tagFilter = "";
        }else{
            tagFilter = "&tags="+g_tagFilter;
        }
        $.ajax({
            type : "POST",
            contentType : "application/json; charset=utf-8",
            url : "/dianping/search?keyword="+keyword+"&longitude="+longitude+"&latitude="+latitude+orderby+categoryFilter+tagFilter,
            success : function(data){
                if(data.rs.flag ){
                    $("#guess").hide();
                    $("#tuanList").hide();
                    $("#searchTitle").show();
                    $("#searchList").show();
                    $("#searchList").empty();
                    $("#slider").hide();
                    var dataList = data.rs.data;
                    var categoryList = data.category;

                    var tagsList = data.tags;
                    for(var i = 0; i < dataList.length; i++){
                        var distance = dataList[i].distance;
                        var distanceStr = "";
                        if(distance >= 1000){
                            distanceStr = getFloat(distance / 1000,1) + " km";
                        }else{
                            distanceStr = distance + " m";
                        }
                        var content = $('<a target="_blank"  href="dianping/details?id='+dataList[i].id+'" class="item Fix hightitem">'+
                            '<div class="cnt">'+
                            '<img class="pic" src="'+dataList[i].logo+'"/>'+
                            '<div class="wrap">'+
                            '<div class="wrap2">'+
                            '<div class="content">'+
                            '<div class="shopname">'+dataList[i].name+'</div><span >'+'</span>'+
                            '<div class="title"><span>店内热卖：</span>' +dataList[i].hot +'</div>'+

                            '<div class="info">'+
                            '<span class="symbol">¥</span>'+
                            '<span class="price">'+dataList[i].pricePerMan+'</span>&nbsp &nbsp &nbsp<span>评分</span>&nbsp<span class="price">'+dataList[i].remarkScore+'</span><span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp距离:'+distanceStr+'</span></div> </div> </div> </div> </div></a>');
                        $("#searchList").append(content);
                    }
                    $("#categoryFilter").empty();
                    var categoryTag = $("<span class='bubble' data-id='"+0+"' id='category"+0+"'>全部</span>");
                    $("#categoryFilter").append(categoryTag);
                    for(var i = 0; i < categoryList.length; i++){
                        var categoryTag = $("<span class='bubble' data-id='"+categoryList[i].id+"' id='category"+categoryList[i].id+"'>"+categoryList[i].name+"</span>");
                        $("#categoryFilter").append(categoryTag);
                    }


                    $("span[id^='category']").on('click',function(){
                        var id = $(this).data("id");
                        if(id == 0){
                            g_categoryFilter = null;
                        }else{
                            g_categoryFilter = id;
                        }
                        searchMethod();
                    });

                    $("#priceFilter").empty();
                    $("#priceFilter").append($('<span class="bubble" id="defaultOrder">默认排序</span>'));
                    $("#priceFilter").append($('<span class="bubble" id="lowPriceOrder">低价排序</span>'));

                    $("#defaultOrder").on('click',function(){
                        g_orderby=null;
                        searchMethod();
                    });
                    $("#lowPriceOrder").on('click',function(){
                        g_orderby=1;
                        searchMethod();
                    });

                    $("#tagsFilter").empty();
                    var tagTag = $("<span class='bubble' data-id='' id='tags'>全部</span>");
                    $("#tagsFilter").append(tagTag);
                    for(var i = 0; i < tagsList.length; i++){


                        var tagTag = $("<span class='bubble' data-id='"+tagsList[i].tags+"' id='tags"+tagsList[i].tags+"'>"+tagsList[i].tags+"("+tagsList[i].num+")</span>");
                        $("#tagsFilter").append(tagTag);


                    }
                    $("span[id^='tags']").on('click',function(){
                        var id = $(this).data("id");
                        if(id == ''){
                            g_tagFilter = null;
                        }else{
                            g_tagFilter = id;
                        }
                        searchMethod();
                    });
                }
            },

        });
    }

    $('#keyword').on('keypress',function(event){
        if(event.keyCode == 13) {
            searchMethod();
        }
    });
    $('#gosearch').on('click',function(event){

        searchMethod();

    });

    $("#resTitle").on("click",function(){
        $("#extraPadding").toggle(100);
    });

});
