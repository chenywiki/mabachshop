import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/couponHistory/list'),
    method:'get',
    params:params
  })
}
