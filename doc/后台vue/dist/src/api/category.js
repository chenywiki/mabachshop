import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/category/list'),
    method:'get',
    params:params
  })
}
export function createBrand(data) {
  return http({
    url:http.adornUrl('/category/save'),
    method:'post',
    data:data
  })
}




export function deleteBrand(id) {
  return http({
    url:http.adornUrl('/category/delete/'+id),
    method:'get',
  })
}

export function getBrand(id) {
  return http({
    url:http.adornUrl('/category/'+id),
    method:'get',
  })
}

export function updateBrand(id,data) {
  return http({
    url:http.adornUrl('/category/update/'+id),
    method:'post',
    data:data
  })
}

