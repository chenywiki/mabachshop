import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/home/newProduct/list'),
    method:'get',
    params:params
  })
}

export function updateRecommendStatus(params) {
  return http({
    url:http.adornUrl('/home/newProduct/update/recommendStatus'),
    method:'post',
    params:params
  })
}

export function deleteNewProduct(params) {
  return http({
    url:http.adornUrl('/home/newProduct/delete'),
    method:'post',
    params:params
  })
}

export function createNewProduct(data) {
  return http({
    url:http.adornUrl('/home/newProduct/create'),
    method:'post',
    data:data
  })
}

export function updateNewProductSort(params) {
  return http({
    url:http.adornUrl('/home/newProduct/update/sort/'+params.id),
    method:'post',
    params:params
  })
}
