import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/member/list'),
    method:'get',
    params:params
  })
}
export function createMember(data) {
  return http({
    url:http.adornUrl('/member/create'),
    method:'post',
    data:data
  })
}
export function updateShowStatus(data) {
  return http({
    url:http.adornUrl('/member/update/showStatus'),
    method:'post',
    data:data
  })
}

export function updateFactoryStatus(data) {
  return http({
    url:http.adornUrl('/member/update/factoryStatus'),
    method:'post',
    data:data
  })
}

export function deleteMember(id) {
  return http({
    url:http.adornUrl('/member/delete/'+id),
    method:'get',
  })
}

export function getMember(id) {
  return http({
    url:http.adornUrl('/member/'+id),
    method:'get',
  })
}

export function updateMember(id,data) {
  return http({
    url:http.adornUrl('/member/update/'+id),
    method:'post',
    data:data
  })
}

