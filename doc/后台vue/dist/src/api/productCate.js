import http from '@/utils/httpRequest'
export function fetchList(parentId,params) {
  return http({
    url:http.adornUrl('/productCategory/list/'+parentId),
    method:'get',
    params:params
  })
}
export function deleteProductCate(id) {
  return http({
    url:http.adornUrl('/productCategory/delete/'+id),
    method:'post'
  })
}

export function createProductCate(data) {
  return http({
    url:http.adornUrl('/productCategory/create'),
    method:'post',
    data:data
  })
}

export function updateProductCate(id,data) {
  return http({
    url:http.adornUrl('/productCategory/update/'+id),
    method:'post',
    data:data
  })
}

export function getProductCate(id) {
  return http({
    url:http.adornUrl('/productCategory/'+id),
    method:'get',
  })
}

export function updateShowStatus(data) {
  return http({
    url:http.adornUrl('/productCategory/update/showStatus'),
    method:'post',
    params:data
  })
}

export function updateNavStatus(data) {
  return http({
    url:http.adornUrl('/productCategory/update/navStatus'),
    method:'post',
    params:data
  })
}

export function fetchListWithChildren() {
  return http({
    url:http.adornUrl('/productCategory/list/withChildren'),
    method:'get'
  })
}
