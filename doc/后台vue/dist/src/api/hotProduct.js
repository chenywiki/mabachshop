import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/home/recommendProduct/list'),
    method:'get',
    params:params
  })
}

export function updateRecommendStatus(params) {
  return http({
    url:http.adornUrl('/home/recommendProduct/update/recommendStatus'),
    method:'post',
    params:params
  })
}

export function deleteHotProduct(params) {
  return http({
    url:http.adornUrl('/home/recommendProduct/delete'),
    method:'post',
    params:params
  })
}

export function createHotProduct(data) {
  return http({
    url:http.adornUrl('/home/recommendProduct/create'),
    method:'post',
    data:data
  })
}

export function updateHotProductSort(params) {
  return http({
    url:http.adornUrl('/home/recommendProduct/update/sort/'),
    method:'post',
    params:params
  })
}
