import http from '@/utils/httpRequest'
export function fetchList(cid,params) {
  return http({
    url:http.adornUrl('/productAttribute/list/'+cid),
    method:'get',
    params:params
  })
}

export function deleteProductAttr(params) {
  return http({
    url:http.adornUrl('/productAttribute/delete'),
    method:'post',
    params:params
  })
}

export function createProductAttr(data) {
  return http({
    url:http.adornUrl('/productAttribute/create'),
    method:'post',
    data:data
  })
}

export function updateProductAttr(id,data) {
  return http({
    url:http.adornUrl('/productAttribute/update/'+id),
    method:'post',
    data:data
  })
}
export function getProductAttr(id) {
  return http({
    url:http.adornUrl('/productAttribute/'+id),
    method:'get'
  })
}

export function getProductAttrInfo(productCategoryId) {
  return http({
    url:http.adornUrl('/productAttribute/attrInfo/'+productCategoryId),
    method:'get'
  })
}
