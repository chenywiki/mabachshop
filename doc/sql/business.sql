/*
SQLyog Ultimate v8.32 
MySQL - 5.7.21 : Database - business
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`business` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `business`;

/*Table structure for table `sms_coupon` */

DROP TABLE IF EXISTS `sms_coupon`;

CREATE TABLE `sms_coupon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` int(1) DEFAULT NULL COMMENT '优惠卷类型；0->全场赠券；1->会员赠券；2->购物赠券；3->注册赠券',
  `name` varchar(100) DEFAULT NULL,
  `platform` int(1) DEFAULT NULL COMMENT '使用平台：0->全部；1->移动；2->PC',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `per_limit` int(11) DEFAULT NULL COMMENT '每人限领张数',
  `min_point` decimal(10,2) DEFAULT NULL COMMENT '使用门槛；0表示无门槛',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `use_type` int(1) DEFAULT NULL COMMENT '使用类型：1->全场通用；1->指定分类；2->指定商品',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  `publish_count` int(11) DEFAULT NULL COMMENT '发行数量',
  `use_count` int(11) DEFAULT NULL COMMENT '已使用数量',
  `receive_count` int(11) DEFAULT NULL COMMENT '领取数量',
  `enable_time` datetime DEFAULT NULL COMMENT '可以领取的日期',
  `code` varchar(64) DEFAULT NULL COMMENT '优惠码',
  `member_level` int(1) DEFAULT NULL COMMENT '可领取的会员类型：0->无限时',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='优惠卷表';

/*Data for the table `sms_coupon` */

insert  into `sms_coupon`(`id`,`type`,`name`,`platform`,`count`,`amount`,`per_limit`,`min_point`,`start_time`,`end_time`,`use_type`,`note`,`publish_count`,`use_count`,`receive_count`,`enable_time`,`code`,`member_level`) values (2,0,'全品类通用券',0,92,'10.00',2,'100.00','2020-01-03 00:00:00','2020-01-23 00:00:00',0,'满100减10',100,0,8,'2018-08-27 16:40:47',NULL,NULL),(3,0,'小米手机专用券',0,92,'50.00',1,'1000.00','2018-08-27 16:40:47','2018-11-16 16:40:47',1,'小米手机专用优惠券',100,0,8,'2018-08-27 16:40:47',NULL,NULL),(4,0,'手机品类专用券',0,92,'300.00',1,'2000.00','2018-08-27 16:40:47','2018-09-15 16:40:47',1,'手机分类专用优惠券',100,0,8,'2018-08-27 16:40:47',NULL,NULL),(7,0,'T恤分类专用优惠券',0,93,'50.00',1,'500.00','2018-08-27 16:40:47','2018-08-15 16:40:47',1,'满500减50',100,0,7,'2018-08-27 16:40:47',NULL,NULL),(8,0,'新优惠券',0,100,'100.00',1,'1000.00','2018-11-08 00:00:00','2018-11-27 00:00:00',0,'测试',100,0,1,NULL,NULL,NULL),(9,0,'全品类通用券',0,100,'5.00',1,'100.00','2018-11-08 00:00:00','2018-11-10 00:00:00',0,NULL,100,0,1,NULL,NULL,NULL),(10,0,'全品类通用券',0,100,'15.00',1,'200.00','2018-11-08 00:00:00','2018-11-10 00:00:00',0,NULL,100,0,1,NULL,NULL,NULL),(11,0,'全品类通用券',0,1000,'50.00',1,'1000.00','2018-11-08 00:00:00','2018-11-10 00:00:00',0,NULL,1000,0,0,NULL,NULL,NULL),(12,0,'移动端全品类通用券',1,1,'10.00',1,'100.00','2018-11-08 00:00:00','2018-11-10 00:00:00',0,NULL,100,0,0,NULL,NULL,NULL),(20,0,'小米手机专用',0,100,'200.00',1,'1000.00','2018-11-09 00:00:00','2018-11-24 00:00:00',1,'小米手机专用',100,0,0,NULL,NULL,NULL),(21,0,'xxx',0,100,'10.00',1,'100.00','2018-11-09 00:00:00','2018-11-30 00:00:00',1,NULL,100,0,0,NULL,NULL,NULL),(22,0,'22222',0,NULL,'11.00',1,'1111.00','2020-01-15 00:00:00','2020-01-16 00:00:00',0,NULL,11,0,0,NULL,NULL,NULL),(23,1,'1111111111111',0,NULL,'11.00',1,'1111.00','2020-01-21 00:00:00','2020-01-31 00:00:00',1,NULL,11,0,0,NULL,NULL,NULL);

/*Table structure for table `sms_coupon_history` */

DROP TABLE IF EXISTS `sms_coupon_history`;

CREATE TABLE `sms_coupon_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `coupon_id` bigint(20) DEFAULT NULL,
  `member_id` bigint(20) DEFAULT NULL,
  `coupon_code` varchar(64) DEFAULT NULL,
  `member_nickname` varchar(64) DEFAULT NULL COMMENT '领取人昵称',
  `get_type` int(1) DEFAULT NULL COMMENT '获取类型：0->后台赠送；1->主动获取',
  `create_time` datetime DEFAULT NULL,
  `use_status` int(1) DEFAULT NULL COMMENT '使用状态：0->未使用；1->已使用；2->已过期',
  `use_time` datetime DEFAULT NULL COMMENT '使用时间',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单编号',
  `order_sn` varchar(100) DEFAULT NULL COMMENT '订单号码',
  PRIMARY KEY (`id`),
  KEY `idx_member_id` (`member_id`) USING BTREE,
  KEY `idx_coupon_id` (`coupon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='优惠券使用、领取历史表';

/*Data for the table `sms_coupon_history` */

insert  into `sms_coupon_history`(`id`,`coupon_id`,`member_id`,`coupon_code`,`member_nickname`,`get_type`,`create_time`,`use_status`,`use_time`,`order_id`,`order_sn`) values (2,2,1,'4931048380330002','windir',1,'2018-08-29 14:04:12',0,'2019-03-21 15:03:40',12,'201809150101000001'),(3,3,1,'4931048380330003','windir',1,'2018-08-29 14:04:29',0,NULL,NULL,NULL),(4,4,1,'4931048380330004','windir',1,'2018-08-29 14:04:32',0,NULL,NULL,NULL),(11,7,1,'4931048380330001','windir',1,'2018-09-04 16:21:50',0,NULL,NULL,NULL),(12,2,4,'0340981248320004','zhensan',1,'2018-11-12 14:16:50',0,NULL,NULL,NULL),(13,3,4,'0342977234360004','zhensan',1,'2018-11-12 14:17:10',0,NULL,NULL,NULL),(14,4,4,'0343342928830004','zhensan',1,'2018-11-12 14:17:13',0,NULL,NULL,NULL),(15,2,5,'0351883832180005','lisi',1,'2018-11-12 14:18:39',0,NULL,NULL,NULL),(16,3,5,'0352201672680005','lisi',1,'2018-11-12 14:18:42',0,NULL,NULL,NULL),(17,4,5,'0352505810180005','lisi',1,'2018-11-12 14:18:45',0,NULL,NULL,NULL),(18,2,6,'0356114588380006','wangwu',1,'2018-11-12 14:19:21',0,NULL,NULL,NULL),(19,3,6,'0356382856920006','wangwu',1,'2018-11-12 14:19:24',0,NULL,NULL,NULL),(20,4,6,'0356656798470006','wangwu',1,'2018-11-12 14:19:27',0,NULL,NULL,NULL),(21,2,3,'0363644984620003','windy',1,'2018-11-12 14:20:36',0,NULL,NULL,NULL),(22,3,3,'0363932820300003','windy',1,'2018-11-12 14:20:39',0,NULL,NULL,NULL),(23,4,3,'0364238275840003','windy',1,'2018-11-12 14:20:42',0,NULL,NULL,NULL),(24,2,7,'0385034833070007','lion',1,'2018-11-12 14:24:10',0,NULL,NULL,NULL),(25,3,7,'0385350208650007','lion',1,'2018-11-12 14:24:13',0,NULL,NULL,NULL),(26,4,7,'0385632733900007','lion',1,'2018-11-12 14:24:16',0,NULL,NULL,NULL),(27,2,8,'0388779132990008','shari',1,'2018-11-12 14:24:48',0,NULL,NULL,NULL),(28,3,8,'0388943658810008','shari',1,'2018-11-12 14:24:49',0,NULL,NULL,NULL),(29,4,8,'0389069398320008','shari',1,'2018-11-12 14:24:51',0,NULL,NULL,NULL),(30,2,9,'0390753935250009','aewen',1,'2018-11-12 14:25:08',0,NULL,NULL,NULL),(31,3,9,'0390882954470009','aewen',1,'2018-11-12 14:25:09',0,NULL,NULL,NULL),(32,4,9,'0391025542810009','aewen',1,'2018-11-12 14:25:10',0,NULL,NULL,NULL);

/*Table structure for table `sms_coupon_product_category_relation` */

DROP TABLE IF EXISTS `sms_coupon_product_category_relation`;

CREATE TABLE `sms_coupon_product_category_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `coupon_id` bigint(20) DEFAULT NULL,
  `product_category_id` bigint(20) DEFAULT NULL,
  `product_category_name` varchar(200) DEFAULT NULL COMMENT '产品分类名称',
  `parent_category_name` varchar(200) DEFAULT NULL COMMENT '父分类名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='优惠券和产品分类关系表';

/*Data for the table `sms_coupon_product_category_relation` */

insert  into `sms_coupon_product_category_relation`(`id`,`coupon_id`,`product_category_id`,`product_category_name`,`parent_category_name`) values (4,19,30,'手机配件','手机数码'),(5,NULL,269,'女性护理','个护化妆'),(6,NULL,269,'女性护理','个护化妆');

/*Table structure for table `sms_coupon_product_relation` */

DROP TABLE IF EXISTS `sms_coupon_product_relation`;

CREATE TABLE `sms_coupon_product_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `coupon_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `product_name` varchar(500) DEFAULT NULL COMMENT '商品名称',
  `product_sn` varchar(200) DEFAULT NULL COMMENT '商品编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='优惠券和产品的关系表';

/*Data for the table `sms_coupon_product_relation` */

insert  into `sms_coupon_product_relation`(`id`,`coupon_id`,`product_id`,`product_name`,`product_sn`) values (9,21,33,'小米（MI）小米电视4A ','4609652');

/*Table structure for table `sms_flash_promotion` */

DROP TABLE IF EXISTS `sms_flash_promotion`;

CREATE TABLE `sms_flash_promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `start_date` date DEFAULT NULL COMMENT '开始日期',
  `end_date` date DEFAULT NULL COMMENT '结束日期',
  `status` int(1) DEFAULT NULL COMMENT '上下线状态',
  `create_time` datetime DEFAULT NULL COMMENT '秒杀时间段名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='限时购表';

/*Data for the table `sms_flash_promotion` */

insert  into `sms_flash_promotion`(`id`,`title`,`start_date`,`end_date`,`status`,`create_time`) values (2,'春季家电家具疯狂秒杀3','2019-01-28','2019-01-15',1,'2018-11-16 11:12:13'),(3,'前端测试专用活动','2018-11-03','2019-02-28',1,'2018-11-16 11:11:31'),(4,'春季家电家具疯狂秒杀3','2018-11-24','2018-11-25',1,'2018-11-16 11:12:19'),(5,'春季家电家具疯狂秒杀4','2018-11-16','2018-11-16',1,'2018-11-16 11:12:24'),(7,'春季家电家具疯狂秒杀6','2018-11-16','2018-11-16',0,'2018-11-16 11:12:35'),(8,'春季家电家具疯狂秒杀7','2018-11-16','2018-11-16',0,'2018-11-16 11:12:39'),(9,'春季家电家具疯狂秒杀8','2018-11-16','2018-11-16',0,'2018-11-16 11:12:42'),(13,'测试','2018-11-01','2018-11-30',0,'2018-11-19 10:34:24'),(14,'uii',NULL,NULL,0,NULL),(18,'sssss',NULL,NULL,0,'2020-01-01 15:24:39'),(19,'111',NULL,NULL,0,'2020-01-01 15:29:44'),(20,'测试','2020-01-02','2020-01-04',0,'2020-01-02 18:32:58'),(21,'ccccccccc','2020-01-03','2020-01-04',0,'2020-01-02 18:34:57'),(22,'nnnn','2020-01-02','2020-01-04',0,'2020-01-02 18:43:23'),(23,'fff1','2020-01-02','2020-01-04',0,'2020-01-02 18:51:40');

/*Table structure for table `sms_flash_promotion_log` */

DROP TABLE IF EXISTS `sms_flash_promotion_log`;

CREATE TABLE `sms_flash_promotion_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `member_phone` varchar(64) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `subscribe_time` datetime DEFAULT NULL COMMENT '会员订阅时间',
  `send_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='限时购通知记录';

/*Data for the table `sms_flash_promotion_log` */

/*Table structure for table `sms_flash_promotion_product_relation` */

DROP TABLE IF EXISTS `sms_flash_promotion_product_relation`;

CREATE TABLE `sms_flash_promotion_product_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `flash_promotion_id` bigint(20) DEFAULT NULL,
  `flash_promotion_session_id` bigint(20) DEFAULT NULL COMMENT '编号',
  `product_id` bigint(20) DEFAULT NULL,
  `flash_promotion_price` decimal(10,2) DEFAULT NULL COMMENT '限时购价格',
  `flash_promotion_count` int(11) DEFAULT NULL COMMENT '限时购数量',
  `flash_promotion_limit` int(11) DEFAULT NULL COMMENT '每人限购数量',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='商品限时购与商品关系表';

/*Data for the table `sms_flash_promotion_product_relation` */

insert  into `sms_flash_promotion_product_relation`(`id`,`flash_promotion_id`,`flash_promotion_session_id`,`product_id`,`flash_promotion_price`,`flash_promotion_count`,`flash_promotion_limit`,`sort`) values (1,2,1,26,'3000.00',10,1,0),(2,2,1,27,'2000.00',10,1,20),(3,2,1,28,'599.00',19,1,0),(4,2,1,29,'4999.00',10,1,100),(9,2,2,26,'2999.00',100,1,0),(10,2,2,27,NULL,NULL,NULL,NULL),(11,2,2,28,NULL,NULL,NULL,NULL),(12,2,2,29,NULL,NULL,NULL,NULL),(13,2,2,30,NULL,NULL,NULL,NULL),(14,2,3,31,NULL,NULL,NULL,NULL),(15,2,3,32,NULL,NULL,NULL,NULL),(16,2,4,33,NULL,NULL,NULL,NULL),(17,2,4,34,NULL,NULL,NULL,NULL),(18,2,5,36,NULL,NULL,NULL,NULL),(19,2,6,33,NULL,NULL,NULL,NULL),(20,2,6,34,NULL,NULL,NULL,NULL),(21,3,1,26,'3000.00',100,1,NULL),(22,3,1,27,'1999.00',10,1,NULL),(23,3,1,28,'599.00',10,1,NULL),(24,3,1,29,'4999.00',10,1,NULL),(25,3,2,31,'90.00',10,1,NULL),(26,3,2,32,'60.00',10,1,NULL),(27,3,2,33,'2299.00',10,1,NULL),(28,3,2,34,'3888.00',10,1,NULL),(29,3,3,36,NULL,NULL,NULL,NULL),(30,3,3,35,NULL,NULL,NULL,NULL),(31,3,3,31,NULL,NULL,NULL,NULL),(32,3,3,32,NULL,NULL,NULL,NULL),(33,3,4,26,NULL,NULL,NULL,NULL),(34,3,4,27,NULL,NULL,NULL,NULL),(35,3,4,28,NULL,NULL,NULL,NULL),(36,3,4,29,NULL,NULL,NULL,NULL),(37,3,5,26,'3688.00',100,1,NULL),(38,3,5,27,'2599.00',10,1,NULL),(39,3,5,28,'599.00',10,1,NULL),(40,3,5,29,'4999.00',10,1,NULL),(41,3,6,26,NULL,NULL,NULL,NULL),(42,3,6,27,NULL,NULL,NULL,NULL),(43,3,6,28,NULL,NULL,NULL,NULL),(44,3,6,29,NULL,NULL,NULL,NULL),(45,2,1,62673800,'1.00',1,1,1),(47,2,1,67046900,NULL,NULL,NULL,NULL),(48,2,1,72266700,'1.00',1,1,1),(54,2,1,72268100,'1.00',100,1,1),(64,2,1,62673800,NULL,NULL,NULL,NULL),(65,2,1,101333000,NULL,NULL,NULL,NULL);

/*Table structure for table `sms_flash_promotion_session` */

DROP TABLE IF EXISTS `sms_flash_promotion_session`;

CREATE TABLE `sms_flash_promotion_session` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(200) DEFAULT NULL COMMENT '场次名称',
  `start_time` time DEFAULT NULL COMMENT '每日开始时间',
  `end_time` time DEFAULT NULL COMMENT '每日结束时间',
  `status` int(1) DEFAULT NULL COMMENT '启用状态：0->不启用；1->启用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='限时购场次表';

/*Data for the table `sms_flash_promotion_session` */

insert  into `sms_flash_promotion_session`(`id`,`name`,`start_time`,`end_time`,`status`,`create_time`) values (1,'8:00','17:57:22','17:57:27',0,'2018-11-16 13:22:17'),(2,'10:00','10:00:00','12:00:00',1,'2018-11-16 13:22:34'),(3,'12:00','12:00:00','14:00:00',1,'2018-11-16 13:22:37'),(4,'14:00','14:00:00','16:00:00',1,'2018-11-16 13:22:41'),(5,'16:00','16:00:00','18:00:00',1,'2018-11-16 13:22:45'),(6,'18:00','18:00:00','20:00:00',1,'2018-11-16 13:21:34'),(7,'20:00','20:00:33','21:00:33',1,'2018-11-16 13:22:55'),(8,'测试','17:58:38','17:58:41',1,NULL);

/*Table structure for table `sms_home_advertise` */

DROP TABLE IF EXISTS `sms_home_advertise`;

CREATE TABLE `sms_home_advertise` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `type` int(1) DEFAULT NULL COMMENT '轮播位置：0->PC首页轮播；1->app首页轮播',
  `pic` varchar(500) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '上下线状态：0->下线；1->上线',
  `click_count` int(11) DEFAULT NULL COMMENT '点击数',
  `order_count` int(11) DEFAULT NULL COMMENT '下单数',
  `url` varchar(500) DEFAULT NULL COMMENT '链接地址',
  `note` varchar(500) DEFAULT NULL COMMENT '备注',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='首页轮播广告表';

/*Data for the table `sms_home_advertise` */

insert  into `sms_home_advertise`(`id`,`name`,`type`,`pic`,`start_time`,`end_time`,`status`,`click_count`,`order_count`,`url`,`note`,`sort`) values (2,'夏季大热促销',1,'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/xiaomi.jpg','2018-11-01 14:01:37','2018-11-15 14:01:37',1,0,0,'#','夏季大热促销',0),(3,'夏季大热促销1',1,'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/5ac1bf58Ndefaac16.jpg','2018-11-13 14:01:37','2018-11-13 14:01:37',1,0,0,NULL,'夏季大热促销1',0),(4,'夏季大热促销2',1,'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/5a9d248cN071f4959.jpg','2018-11-13 14:01:37','2018-11-13 14:01:37',1,0,0,NULL,'夏季大热促销2',0),(9,'电影推荐广告',1,'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20181113/movie_ad.jpg','2018-11-01 00:00:00','2018-11-24 00:00:00',1,0,0,'www.baidu.com','电影推荐广告',100),(10,'汽车促销广告',1,'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20181113/car_ad.jpg','2018-11-13 00:00:00','2018-11-24 00:00:00',1,0,0,'xxx',NULL,99),(11,'汽车推荐广告',1,'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20181113/car_ad2.jpg','2018-11-13 00:00:00','2018-11-30 00:00:00',1,0,0,'xxx',NULL,98);

/*Table structure for table `sms_home_brand` */

DROP TABLE IF EXISTS `sms_home_brand`;

CREATE TABLE `sms_home_brand` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) DEFAULT NULL,
  `brand_name` varchar(64) DEFAULT NULL,
  `recommend_status` int(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='首页推荐品牌表';

/*Data for the table `sms_home_brand` */

insert  into `sms_home_brand`(`id`,`brand_id`,`brand_name`,`recommend_status`,`sort`) values (42,7174,'富可视',1,NULL),(43,15127,'三星',1,NULL),(44,1,'万和',1,NULL),(45,2,'三星',1,NULL),(46,15539,'神舟',1,NULL),(47,16506,'索爱',1,NULL),(48,16538,'索尼',1,NULL),(49,16975,'天语',1,NULL),(50,17193,'外星人',NULL,NULL),(51,17424,'威图',NULL,NULL),(52,17440,'微软',NULL,NULL),(53,18078,'NAMO',NULL,NULL),(54,18362,'小辣椒',NULL,NULL),(55,18374,'小米',NULL,NULL),(56,1,'万和',NULL,NULL),(57,2,'三星',NULL,NULL),(58,3,'华为',NULL,NULL),(59,4,'格力',NULL,NULL),(60,5,'方太',NULL,NULL),(61,1,'万和',NULL,NULL),(62,2,'三星',NULL,NULL),(63,3,'华为',NULL,NULL),(64,4,'格力',NULL,NULL),(65,5,'方太',NULL,NULL);

/*Table structure for table `sms_home_new_product` */

DROP TABLE IF EXISTS `sms_home_new_product`;

CREATE TABLE `sms_home_new_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL,
  `product_name` varchar(64) DEFAULT NULL,
  `recommend_status` int(1) DEFAULT NULL,
  `sort` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='新鲜好物表';

/*Data for the table `sms_home_new_product` */

insert  into `sms_home_new_product`(`id`,`product_id`,`product_name`,`recommend_status`,`sort`) values (2,27,'小米8 全面屏游戏智能手机 6GB+64GB 黑色 全网通4G 双卡双待',1,1),(8,26,'华为 HUAWEI P20 ',1,0),(9,27,'小米8 全面屏游戏智能手机 6GB+64GB 黑色 全网通4G 双卡双待',1,0),(10,28,'小米 红米5A 全网通版 3GB+32GB 香槟金 移动联通电信4G手机 双卡双待',1,0),(11,29,'Apple iPhone 8 Plus 64GB 红色特别版 移动联通电信4G手机',1,0),(12,30,'HLA海澜之家简约动物印花短袖T恤',1,0),(13,62673800,'花王（Merries）拉拉裤 M58片 中号尿不湿（6-11kg）（日本原装进口）',1,NULL),(14,67046900,'德国进口 甘蒂牧场（MUH）牧牌 部分脱脂纯牛奶1L*6盒进口纯牛奶 礼盒装',1,NULL),(15,72266700,'LATIT PC拉链旅行行李箱 拉杆箱 旅行箱 20英寸 万向轮 黑色',1,NULL),(19,79887200,'德国进口 甘蒂牧场（MUH）牧牌 部分脱脂纯牛奶1L*12盒 进口纯牛奶 整箱',1,NULL),(20,79887300,'德国进口 甘蒂牧场（MUH）牧牌 全脂纯牛奶 200ml*10盒 进口纯牛奶 礼盒装',NULL,NULL),(21,85718700,'莎米特SUMMIT 旅行拉杆箱28英寸PC材质大容量旅行行李箱PC154 {颜色}',NULL,NULL),(22,89342500,'瑞动拉杆箱24英寸旅行箱 时尚轻巧行李箱静音万向轮男女 5019深灰拉丝',NULL,NULL),(23,62673800,'花王（Merries）拉拉裤 M58片 中号尿不湿（6-11kg）（日本原装进口）',NULL,NULL),(24,67046900,'德国进口 甘蒂牧场（MUH）牧牌 部分脱脂纯牛奶1L*6盒进口纯牛奶 礼盒装',NULL,NULL),(25,72266700,'LATIT PC拉链旅行行李箱 拉杆箱 旅行箱 20英寸 万向轮 黑色',NULL,NULL),(26,72268100,'LATIT PC铝框旅行行李箱 拉杆箱 旅行箱  24英寸 万向轮 亮黑色',NULL,NULL),(27,75402400,'美旅拉杆箱 24英寸男女超轻大容量行李箱耐磨万向轮托运箱送箱套 密码锁旅行箱40T{颜色}',NULL,NULL),(28,75480100,'德国进口牛奶 德亚（Weidendorf）全脂纯牛奶 200ml*12盒 礼盒装',NULL,NULL),(29,79887200,'德国进口 甘蒂牧场（MUH）牧牌 部分脱脂纯牛奶1L*12盒 进口纯牛奶 整箱',NULL,NULL),(30,79887300,'德国进口 甘蒂牧场（MUH）牧牌 全脂纯牛奶 200ml*10盒 进口纯牛奶 礼盒装',NULL,NULL),(31,85718700,'莎米特SUMMIT 旅行拉杆箱28英寸PC材质大容量旅行行李箱PC154 {颜色}',NULL,NULL),(32,89342500,'瑞动拉杆箱24英寸旅行箱 时尚轻巧行李箱静音万向轮男女 5019深灰拉丝',NULL,NULL);

/*Table structure for table `sms_home_recommend_product` */

DROP TABLE IF EXISTS `sms_home_recommend_product`;

CREATE TABLE `sms_home_recommend_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL,
  `product_name` varchar(64) DEFAULT NULL,
  `recommend_status` int(1) DEFAULT NULL,
  `sort` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='人气推荐商品表';

/*Data for the table `sms_home_recommend_product` */

insert  into `sms_home_recommend_product`(`id`,`product_id`,`product_name`,`recommend_status`,`sort`) values (3,26,'华为 HUAWEI P20 ',0,23),(4,27,'小米8 全面屏游戏智能手机 6GB+64GB 黑色 全网通4G 双卡双待',1,0),(5,28,'小米 红米5A 全网通版 3GB+32GB 香槟金 移动联通电信4G手机 双卡双待',1,0),(6,29,'Apple iPhone 8 Plus 64GB 红色特别版 移动联通电信4G手机',1,0),(7,30,'HLA海澜之家简约动物印花短袖T恤',1,100),(8,62673800,'花王（Merries）拉拉裤 M58片 中号尿不湿（6-11kg）（日本原装进口）',1,NULL),(9,67046900,'德国进口 甘蒂牧场（MUH）牧牌 部分脱脂纯牛奶1L*6盒进口纯牛奶 礼盒装',1,NULL),(10,72266700,'LATIT PC拉链旅行行李箱 拉杆箱 旅行箱 20英寸 万向轮 黑色',1,NULL),(11,72268100,'LATIT PC铝框旅行行李箱 拉杆箱 旅行箱  24英寸 万向轮 亮黑色',NULL,NULL),(15,79887300,'德国进口 甘蒂牧场（MUH）牧牌 全脂纯牛奶 200ml*10盒 进口纯牛奶 礼盒装',NULL,NULL),(16,85718700,'莎米特SUMMIT 旅行拉杆箱28英寸PC材质大容量旅行行李箱PC154 {颜色}',NULL,NULL),(17,89342500,'瑞动拉杆箱24英寸旅行箱 时尚轻巧行李箱静音万向轮男女 5019深灰拉丝',NULL,NULL),(18,62673800,'花王（Merries）拉拉裤 M58片 中号尿不湿（6-11kg）（日本原装进口）',NULL,NULL),(19,67046900,'德国进口 甘蒂牧场（MUH）牧牌 部分脱脂纯牛奶1L*6盒进口纯牛奶 礼盒装',NULL,NULL),(20,72266700,'LATIT PC拉链旅行行李箱 拉杆箱 旅行箱 20英寸 万向轮 黑色',NULL,NULL),(21,72268100,'LATIT PC铝框旅行行李箱 拉杆箱 旅行箱  24英寸 万向轮 亮黑色',NULL,NULL),(22,75402400,'美旅拉杆箱 24英寸男女超轻大容量行李箱耐磨万向轮托运箱送箱套 密码锁旅行箱40T{颜色}',NULL,NULL);

/*Table structure for table `sms_home_recommend_subject` */

DROP TABLE IF EXISTS `sms_home_recommend_subject`;

CREATE TABLE `sms_home_recommend_subject` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject_id` bigint(20) DEFAULT NULL,
  `subject_name` varchar(64) DEFAULT NULL,
  `recommend_status` int(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='首页推荐专题表';

/*Data for the table `sms_home_recommend_subject` */

insert  into `sms_home_recommend_subject`(`id`,`subject_id`,`subject_name`,`recommend_status`,`sort`) values (14,1,'polo衬衫的也时尚',0,11),(15,2,'大牌手机低价秒',0,1),(16,3,'晓龙845新品上市',0,0),(17,4,'夏天应该穿什么',0,0),(18,5,'夏季精选',0,100),(19,6,'品牌手机降价',1,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
