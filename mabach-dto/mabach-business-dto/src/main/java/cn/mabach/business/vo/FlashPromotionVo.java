package cn.mabach.business.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class FlashPromotionVo {
    /**
     *
     */
    @TableId
    private Long id;
    /**
     *
     */
    private String title;
    /**
     * 开始日期
     */

    private String  startDate;

    /**
     * 结束日期
     */

    private String endDate;

    /**
     * 上下线状态
     */
    private Integer status;
    /**
     * 秒杀时间段名称
     */
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//
//	@JsonDeserialize(using = CustomJsonDateDeserializer.class)
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
