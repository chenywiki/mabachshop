package cn.mabach.business.dto;

import cn.mabach.business.entity.FlashPromotionSessionEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FlashPromotionSessionDetail extends FlashPromotionSessionEntity {
    @ApiModelProperty(value = "产品数量")
    private Long productCount;
}
