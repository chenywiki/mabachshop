package cn.mabach.goods.dto;

import cn.mabach.goods.entity.ProductAttributeCategoryEntity;
import cn.mabach.goods.entity.ProductAttributeEntity;
import lombok.Data;

import java.util.List;

@Data
public class ProductAttributeCategoryItem extends ProductAttributeCategoryEntity {
    private List<ProductAttributeEntity> productAttributeList;
}
