package cn.mabach.goods.dto;


import lombok.Data;

/**
 * 商品分类对应属性信息
 * Created by macro on 2019/12/23.
 */
@Data
public class ProductAttrInfo {
    private Long attributeId;
    private Long attributeCategoryId;

}
