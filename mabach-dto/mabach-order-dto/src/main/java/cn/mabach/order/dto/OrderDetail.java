package cn.mabach.order.dto;

import cn.mabach.order.entity.OrderEntity;
import cn.mabach.order.entity.OrderItemEntity;
import cn.mabach.order.entity.OrderOperateHistoryEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class OrderDetail extends OrderEntity {
    @ApiModelProperty("订单详情列表")
    private List<OrderItemEntity> orderItemList;
    @ApiModelProperty("订单日志")
    private List<OrderOperateHistoryEntity> historyList;
}