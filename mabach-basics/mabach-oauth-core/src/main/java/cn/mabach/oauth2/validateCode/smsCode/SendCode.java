package cn.mabach.oauth2.validateCode.smsCode;

/**
 * @Author: ming
 * @Date: 2020/2/2 0002 下午 5:38
 */
public interface SendCode {

    public void send(String moblie,String code);
}
