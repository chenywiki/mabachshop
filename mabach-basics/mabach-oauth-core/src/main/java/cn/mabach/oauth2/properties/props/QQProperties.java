package cn.mabach.oauth2.properties.props;

import lombok.Data;

/**
 * @Author: ming
 * @Date: 2020/2/4 0004 下午 2:05
 */
@Data
public class QQProperties {
    private String appId="101825845";
    private String loginBackUrl="http://www.mabach.cn/qqloginback";
    private String appKey="b528ba50996e44effe138c303fda4a25";
}
