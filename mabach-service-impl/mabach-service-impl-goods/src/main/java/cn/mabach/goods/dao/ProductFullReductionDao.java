package cn.mabach.goods.dao;


import cn.mabach.goods.entity.ProductFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 产品满减表(只针对同商品)
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:23:55
 */
@Mapper
public interface ProductFullReductionDao extends BaseMapper<ProductFullReductionEntity> {
	
}
