package cn.mabach.goods.feign;

import cn.mabach.cms.service.PrefrenceAreaProductRelationService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-cms")
public interface PrefrenceAreaProductRelationServiceFeign extends PrefrenceAreaProductRelationService {
}
