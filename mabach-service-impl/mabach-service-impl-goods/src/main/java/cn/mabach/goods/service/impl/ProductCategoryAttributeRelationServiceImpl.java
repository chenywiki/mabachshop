package cn.mabach.goods.service.impl;


import cn.mabach.goods.dao.ProductCategoryAttributeRelationDao;
import cn.mabach.goods.entity.ProductCategoryAttributeRelationEntity;
import cn.mabach.goods.service.ProductCategoryAttributeRelationService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class ProductCategoryAttributeRelationServiceImpl extends ServiceImpl<ProductCategoryAttributeRelationDao, ProductCategoryAttributeRelationEntity> implements ProductCategoryAttributeRelationService {

    @Override
    public RS<PageResult<ProductCategoryAttributeRelationEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        if (pageSize>20){
            return RS.error("ProductCategoryAttributeRelationServiceImpl非法参数");
        }
        IPage<ProductCategoryAttributeRelationEntity> page = this.page(
                new Page<ProductCategoryAttributeRelationEntity>(pageNum,pageSize),
                new QueryWrapper<ProductCategoryAttributeRelationEntity>().like("name",keyword)
        );
        log.info("ProductCategoryAttributeRelationServiceImpl:{}",Thread.currentThread().getName());
        return RS.ok(new PageResult(page));
    }





    @Override
    public RS<ProductCategoryAttributeRelationEntity> getByIdE(@RequestParam("id") Long id) {
            ProductCategoryAttributeRelationEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  ProductCategoryAttributeRelationEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateByIdE(@RequestBody ProductCategoryAttributeRelationEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }


    @Override
    public RS removeOneById(@RequestParam("id") Long id) {
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeByIdsE(@RequestBody List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

}