package cn.mabach.goods.dao;


import cn.mabach.goods.entity.ProductCategoryAttributeRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:23:55
 */
@Mapper
public interface ProductCategoryAttributeRelationDao extends BaseMapper<ProductCategoryAttributeRelationEntity> {
	
}
