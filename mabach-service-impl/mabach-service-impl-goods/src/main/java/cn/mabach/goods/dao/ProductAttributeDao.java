package cn.mabach.goods.dao;


import cn.mabach.goods.dto.ProductAttrInfo;
import cn.mabach.goods.entity.ProductAttributeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 商品属性参数表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:24:34
 */
@Mapper
public interface ProductAttributeDao extends BaseMapper<ProductAttributeEntity> {


    @Select("        SELECT " +
            "            pa.id  attributeId," +
            "            pac.id attributeCategoryId " +
            "        FROM " +
            "            tb_product_category_attribute_relation pcar " +
            "            LEFT JOIN tb_product_attribute pa ON pa.id = pcar.product_attribute_id " +
            "            LEFT JOIN tb_product_attribute_category pac ON pa.product_attribute_category_id = pac.id " +
            "        WHERE " +
            "            pcar.product_category_id = #{id}")
    List<ProductAttrInfo> getProductAttrInfo(@Param("id") Long id);
	
}
