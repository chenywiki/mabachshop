

package cn.mabach;


import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2Doc
@EnableCaching
@EnableFeignClients
//@EnableDistributedTransaction
@MapperScan(basePackages = "cn.mabach.goods.dao")
public class GoodsApp {

	public static void main(String[] args) {
		SpringApplication.run(GoodsApp.class, args);
	}

}