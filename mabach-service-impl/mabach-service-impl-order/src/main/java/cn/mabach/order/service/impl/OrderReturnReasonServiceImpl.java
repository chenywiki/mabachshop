package cn.mabach.order.service.impl;



import cn.mabach.order.dao.OrderReturnReasonDao;
import cn.mabach.order.entity.OrderReturnReasonEntity;
import cn.mabach.order.service.OrderReturnReasonService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderReturnReasonServiceImpl extends ServiceImpl<OrderReturnReasonDao, OrderReturnReasonEntity> implements OrderReturnReasonService {

    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<OrderReturnReasonEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                             @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<OrderReturnReasonEntity> page = this.page(
                new Page<OrderReturnReasonEntity>(pageNum,pageSize),
                new QueryWrapper<OrderReturnReasonEntity>()
        );

        return RS.ok(new PageResult(page));
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<OrderReturnReasonEntity> getByIdE(@RequestParam("id") Long id) {
            OrderReturnReasonEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  OrderReturnReasonEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody OrderReturnReasonEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id删除
     * */
    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
    * 修改退货原因启用状态
    * */

    @Transactional
    public RS updateStatus(@RequestParam(value = "status") Integer status,
                           @RequestParam("ids") List<Long> ids) {
        OrderReturnReasonEntity reasonEntity = new OrderReturnReasonEntity();
        reasonEntity.setStatus(status);
        boolean b = this.update(reasonEntity, new QueryWrapper<OrderReturnReasonEntity>().in("id", ids));
        if (!b){
            return RS.error();
        }

        return RS.ok();
    }

}