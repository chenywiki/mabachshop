package cn.mabach.order.service.impl;



import cn.mabach.order.dao.CompanyAddressDao;
import cn.mabach.order.entity.CompanyAddressEntity;
import cn.mabach.order.service.CompanyAddressService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class CompanyAddressServiceImpl extends ServiceImpl<CompanyAddressDao, CompanyAddressEntity> implements CompanyAddressService {

    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<CompanyAddressEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<CompanyAddressEntity> page = this.page(
                new Page<CompanyAddressEntity>(pageNum,pageSize),
                new QueryWrapper<CompanyAddressEntity>()
        );

        return RS.ok(new PageResult(page));
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<CompanyAddressEntity> getByIdE(@RequestParam("id") Long id) {
            CompanyAddressEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  CompanyAddressEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody CompanyAddressEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id删除
     * */
    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

}