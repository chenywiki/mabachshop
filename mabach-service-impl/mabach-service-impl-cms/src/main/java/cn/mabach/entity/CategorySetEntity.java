package cn.mabach.entity;

import lombok.Data;

/**
 * @Author: ming
 * @Date: 2020/3/28 0028 下午 4:10
 */
@Data
public class CategorySetEntity {
    private Integer id;
    private String name;
}
