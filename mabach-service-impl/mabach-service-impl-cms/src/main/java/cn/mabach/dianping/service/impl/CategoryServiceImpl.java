package cn.mabach.dianping.service.impl;

import cn.mabach.dianping.dao.CategoryDao;
import cn.mabach.dianping.entity.CategoryEntity;
import cn.mabach.dianping.service.CategoryService;
import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Override
    public PageResult queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        IPage<CategoryEntity> page = this.page(
                new Page<CategoryEntity>(pageNum,pageSize),
                new QueryWrapper<CategoryEntity>().like(!StringUtils.isEmpty(keyword),"name",keyword).orderByDesc("sort")
                .orderByAsc("id")
        );

        return new PageResult(page);
    }

    @Override
    public CategoryEntity getByIdE(Integer id) {
        return this.getById(id);
    }

    @Override
    public RS saveE(CategoryEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();

    }

    @Override
    public RS updateByIdE(CategoryEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeOneById(Integer id) {
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeByIdsE(List<Integer> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

}