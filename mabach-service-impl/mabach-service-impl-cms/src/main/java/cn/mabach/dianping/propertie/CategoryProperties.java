package cn.mabach.dianping.propertie;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: ming
 * @Date: 2020/3/2 0002 下午 3:03
 */
@ConfigurationProperties(prefix = "category")
@Data
public class CategoryProperties {
    List<ListProperties> list=new ArrayList<>();
}
