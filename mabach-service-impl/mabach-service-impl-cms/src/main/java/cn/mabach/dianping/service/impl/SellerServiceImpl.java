package cn.mabach.dianping.service.impl;


import cn.mabach.dianping.dao.SellerDao;
import cn.mabach.dianping.entity.SellerEntity;
import cn.mabach.dianping.service.SellerService;
import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;



@RestController
@Slf4j
public class SellerServiceImpl extends ServiceImpl<SellerDao, SellerEntity> implements SellerService {


    @Override
    public PageResult queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        IPage<SellerEntity> page = this.page(
                new Page<SellerEntity>(pageNum,pageSize),
                new QueryWrapper<SellerEntity>().like(!StringUtils.isEmpty(keyword),"name",keyword)
        );


        return new PageResult(page);
    }

    @Override
    public SellerEntity getByIdE(Integer id) {
        return this.getById(id);
    }

    @Override
    public RS saveE(SellerEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();

    }

    @Override
    public RS updateByIdE(SellerEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeOneById(Integer id) {
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeByIdsE(List<Integer> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateStatus(@RequestParam("id") Integer id,@RequestParam("disabledFlag")Integer disabledFlag) {
        SellerEntity byId = this.getById(id);
        byId.setDisabledFlag(disabledFlag);
        this.updateById(byId);
        return RS.ok();
    }


}