package cn.mabach.business.service.impl;



import cn.mabach.business.dao.HomeAdvertiseDao;
import cn.mabach.business.entity.HomeAdvertiseEntity;
import cn.mabach.business.service.HomeAdvertiseService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class HomeAdvertiseServiceImpl extends ServiceImpl<HomeAdvertiseDao, HomeAdvertiseEntity> implements HomeAdvertiseService {

    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<HomeAdvertiseEntity>> queryPage(@RequestParam(value = "name", required = false) String name,
                                                         @RequestParam(value = "type", required = false) Integer type,
                                                         @RequestParam(value = "endTime", required = false) String endTime,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<HomeAdvertiseEntity> page = this.page(
                new Page<HomeAdvertiseEntity>(pageNum,pageSize),
                new QueryWrapper<HomeAdvertiseEntity>().like(!StringUtils.isEmpty(name),"name",name)
                .eq(type!=null,"type",type)
                .eq(!StringUtils.isEmpty(endTime),"end_time",endTime)

        );

        return RS.ok(new PageResult(page));
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<HomeAdvertiseEntity> getByIdE(@RequestParam("id") Long id) {
            HomeAdvertiseEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  HomeAdvertiseEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody HomeAdvertiseEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id删除
     * */
    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateStatus(@RequestParam("id") Long id, @RequestParam("status") Integer status) {
        HomeAdvertiseEntity homeAdvertiseEntity = new HomeAdvertiseEntity();
        homeAdvertiseEntity.setId(id);
        homeAdvertiseEntity.setStatus(status);
        boolean b = this.updateById(homeAdvertiseEntity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

}