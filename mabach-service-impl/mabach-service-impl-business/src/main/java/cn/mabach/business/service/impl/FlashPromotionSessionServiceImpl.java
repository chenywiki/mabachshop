package cn.mabach.business.service.impl;



import cn.mabach.business.dao.FlashPromotionSessionDao;
import cn.mabach.business.dto.FlashPromotionSessionDetail;
import cn.mabach.business.entity.FlashPromotionSessionEntity;
import cn.mabach.business.service.FlashPromotionSessionService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import cn.mabach.utils.BeanUtilsMabach;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FlashPromotionSessionServiceImpl extends ServiceImpl<FlashPromotionSessionDao, FlashPromotionSessionEntity> implements FlashPromotionSessionService {


    @Autowired
    private FlashPromotionProductRelationServiceImpl flashPromotionProductRelationService;
    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<FlashPromotionSessionEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<FlashPromotionSessionEntity> page = this.page(
                new Page<FlashPromotionSessionEntity>(pageNum,pageSize),
                new QueryWrapper<FlashPromotionSessionEntity>().like(!StringUtils.isEmpty(keyword),"name",keyword)
        );

        return RS.ok(new PageResult(page));
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<FlashPromotionSessionEntity> getByIdE(@RequestParam("id") Long id) {
            FlashPromotionSessionEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  FlashPromotionSessionEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody FlashPromotionSessionEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id删除
     * */
    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateStatus(@RequestParam("id") Long id, @RequestParam("status") Integer status) {
        FlashPromotionSessionEntity entity = new FlashPromotionSessionEntity();
        entity.setId(id);
        entity.setStatus(status);
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS<List<FlashPromotionSessionEntity>> listAll() {

        IPage<FlashPromotionSessionEntity> page = this.page(
                new Page<FlashPromotionSessionEntity>(1,100),
                new QueryWrapper<FlashPromotionSessionEntity>()
        );

        return RS.ok(page.getRecords());

    }

    @Override
    public RS<List<FlashPromotionSessionDetail>> selectList(@RequestParam("flashPromotionId") Long flashPromotionId) {
        ArrayList<FlashPromotionSessionDetail> arrayList = new ArrayList<>();
        List<FlashPromotionSessionEntity> status = this.list(new QueryWrapper<FlashPromotionSessionEntity>().eq("status", 1));
        for (FlashPromotionSessionEntity promotionSessionEntity : status) {
            FlashPromotionSessionDetail flashPromotionSessionDetail = BeanUtilsMabach.doToDto(promotionSessionEntity, FlashPromotionSessionDetail.class);
            long count = flashPromotionProductRelationService.getCount(flashPromotionId, promotionSessionEntity.getId());
            flashPromotionSessionDetail.setProductCount(count);
            arrayList.add(flashPromotionSessionDetail);
        }
        return RS.ok(arrayList);
    }

}