package cn.mabach.member.dao;


import cn.mabach.member.entity.SendMessageEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-02-24 21:58:59
 */
@Mapper
public interface SendMessageDao extends BaseMapper<SendMessageEntity> {
	
}
