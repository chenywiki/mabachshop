package cn.mabach.member.service.impl;




import cn.mabach.member.dao.SendMessageDao;
import cn.mabach.member.entity.SendMessageEntity;
import cn.mabach.member.service.SendMessageService;
import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class SendMessageServiceImpl extends ServiceImpl<SendMessageDao, SendMessageEntity> implements SendMessageService {

    /*
     *分页查询
     * */
    @Override
    public TableDataInfo queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<SendMessageEntity> page = this.page(
                new Page<SendMessageEntity>(pageNum,pageSize),
                new QueryWrapper<SendMessageEntity>().like(!StringUtils.isEmpty(keyword),"name",keyword)
        );

        return new TableDataInfo(page);
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<SendMessageEntity> getByIdE(@RequestParam("id") Long id) {
            SendMessageEntity res = this.getById(id);

        return RS.ok(res);
    }

    /*
     *新增
     * */
    @Override
    public RS saveE(@RequestBody  SendMessageEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody SendMessageEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }



}