package cn.mabach.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * sku的库存
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:28:39
 */
@Data
@TableName("tb_sku_stock")
public class SkuStockEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long productId;
	/**
	 * sku编码
	 */
		private String skuCode;
	/**
	 * 
	 */
		private BigDecimal price;
	/**
	 * 库存
	 */
		private Integer stock;
	/**
	 * 预警库存
	 */
		private Integer lowStock;
	/**
	 * 销售属性1
	 */
		private String sp1;
	/**
	 * 
	 */
		private String sp2;
	/**
	 * 
	 */
		private String sp3;
	/**
	 * 展示图片
	 */
		private String pic;
	/**
	 * 销量
	 */
		private Integer sale;
	/**
	 * 单品促销价格
	 */
		private BigDecimal promotionPrice;
	/**
	 * 锁定库存
	 */
		private Integer lockStock;
	/**
	 * 0：未删除，1：删除
	 */
		private Integer isDelete;

}
