package cn.mabach.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 运费模版
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:29:32
 */
@Data
@TableName("tb_feight_template")
public class FeightTemplateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private String name;
	/**
	 * 计费类型:0->按重量；1->按件数
	 */
		private Integer chargeType;
	/**
	 * 首重kg
	 */
		private BigDecimal firstWeight;
	/**
	 * 首费（元）
	 */
		private BigDecimal firstFee;
	/**
	 * 
	 */
		private BigDecimal continueWeight;
	/**
	 * 
	 */
		private BigDecimal continmeFee;
	/**
	 * 目的地（省、市）
	 */
		private String dest;

}
