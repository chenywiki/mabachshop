package cn.mabach.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 产品阶梯价格表(只针对同商品)
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:28:40
 */
@Data
@TableName("tb_product_ladder")
public class ProductLadderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long productId;
	/**
	 * 满足的商品数量
	 */
		private Integer count;
	/**
	 * 折扣
	 */
		private BigDecimal discount;
	/**
	 * 折后价格
	 */
		private BigDecimal price;

}
