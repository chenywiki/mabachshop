package cn.mabach.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;


import lombok.Data;

/**
 * 商品审核记录
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:28:40
 */
@Data
@TableName("tb_product_vertify_record")
public class ProductVertifyRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long productId;
	/**
	 * 
	 */

	private Date createTime;
	/**
	 * 审核人
	 */
		private String vertifyMan;
	/**
	 * 
	 */
		private Integer status;
	/**
	 * 反馈详情
	 */
		private String detail;

}
