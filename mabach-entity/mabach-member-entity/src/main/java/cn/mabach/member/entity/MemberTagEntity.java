package cn.mabach.member.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户标签表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-04 17:40:29
 */
@Data
@TableName("ums_member_tag")
public class MemberTagEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private String name;
	/**
	 * 自动打标签完成订单数量
	 */
		private Integer finishOrderCount;
	/**
	 * 自动打标签完成订单金额
	 */
		private BigDecimal finishOrderAmount;

}
