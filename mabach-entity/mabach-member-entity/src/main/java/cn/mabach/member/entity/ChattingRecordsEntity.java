package cn.mabach.member.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-02-24 21:58:59
 */
@Data
@TableName("chatting_records")
public class ChattingRecordsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
		@TableId
	
private Long id;
	/**
	 * 发送者ID
	 */
	
private Long sendUserId;
	/**
	 * 接收者id
	 */
	
private Long receiveId;
	/**
	 * 聊天记录
	 */
	
private String message;
	/**
	 * 是否已阅
	 */
	
private Integer signFlag;
	/**
	 * 创建时间
	 */
	
private Date createTime;

}
