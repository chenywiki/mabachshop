package cn.mabach.business.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 首页轮播广告表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 19:26:24
 */
@Data
@TableName("sms_home_advertise")
public class HomeAdvertiseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private String name;
	/**
	 * 轮播位置：0->PC首页轮播；1->app首页轮播
	 */
		private Integer type;
	/**
	 * 
	 */
		private String pic;
	/**
	 * 
	 */

	private Date startTime;
	/**
	 * 
	 */

	private Date endTime;
	/**
	 * 上下线状态：0->下线；1->上线
	 */
		private Integer status;
	/**
	 * 点击数
	 */
		private Integer clickCount;
	/**
	 * 下单数
	 */
		private Integer orderCount;
	/**
	 * 链接地址
	 */
		private String url;
	/**
	 * 备注
	 */
		private String note;
	/**
	 * 排序
	 */
		private Integer sort;

}
