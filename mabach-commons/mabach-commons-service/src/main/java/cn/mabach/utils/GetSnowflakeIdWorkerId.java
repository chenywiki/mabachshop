package cn.mabach.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GetSnowflakeIdWorkerId {
    @Bean
    public SnowflakeIdWorker getId(){
        return new SnowflakeIdWorker(2,1);
    }
}
