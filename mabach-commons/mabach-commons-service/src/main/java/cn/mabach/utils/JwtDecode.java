package cn.mabach.utils;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;

import java.io.IOException;

/**
 * @Author: ming
 * @Date: 2020/2/8 0008 下午 9:49
 */
public class JwtDecode {


    public static String decode(String token){
        String publicKey = null;

        ClassPathResource resource = new ClassPathResource("public.key");

        try {
            publicKey = IOUtils.toString(resource.getInputStream(), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String claims = JwtHelper.decodeAndVerify(token, new RsaVerifier(publicKey)).getClaims();
        return claims;
    }




}
