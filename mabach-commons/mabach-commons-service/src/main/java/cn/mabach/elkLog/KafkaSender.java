//package cn.mabach.elkLog;
//
//
//import com.alibaba.fastjson.JSON;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.support.SendResult;
//import org.springframework.stereotype.Component;
//import org.springframework.util.concurrent.ListenableFuture;
//import org.springframework.util.concurrent.ListenableFutureCallback;
//
///**
// * 生产者
// *
// *
//
// */
//@Component
//@Slf4j
//public class KafkaSender<T> {
//
//	@Autowired
//	private KafkaTemplate<String, Object> kafkaTemplate;
//
//
//
//
//
//	/**
//	 * kafka 发送消息
//	 *
//	 * @param obj
//	 *            消息对象
//	 */
//	public void send(String topic,T obj) {
//		String jsonObj = JSON.toJSONString(obj);
//
//
//		// 发送消息
//		ListenableFuture<SendResult<String, Object>> future = kafkaTemplate.send(topic, jsonObj);
//		future.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
//			@Override
//			public void onFailure(Throwable throwable) {
//
//			}
//
//			@Override
//			public void onSuccess(SendResult<String, Object> stringObjectSendResult) {
//				// TODO 业务处理
//
//
//			}
//		});
//	}
//}