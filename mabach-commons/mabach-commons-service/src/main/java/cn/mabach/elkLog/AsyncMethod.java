//package cn.mabach.elkLog;
//
//import com.alibaba.fastjson.JSONObject;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.scheduling.annotation.AsyncResult;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//
//import javax.servlet.http.HttpServletRequest;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.Future;
//
///**
// * @Author: ming
// * @Date: 2020/2/12 0012 下午 5:02
// */
//@Component
//@Slf4j
//public class AsyncMethod {
//    @Autowired
//    private KafkaSender kafkaSender;
//    private static final String infoTopic="myLog";
//    private static final String errorTopic="errorLog";
//
//    @Async("Log")
//    public void sendLog(HttpServletRequest request, String args, Long time ,Object o,String signature){
//
//
//
//
//
//
//        String name = Thread.currentThread().getName();
//        long id = Thread.currentThread().getId();
//        log.info("副线程name:{},id:{}",name,id);
//        String requestURI = request.getRequestURI();
//        String remoteHost = request.getRemoteHost();
//        log.info("requestURI:{},remotePort:{}",requestURI,remoteHost);
//        HashMap<String, Object> logJson = new HashMap<>();
//        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        logJson.put("type","info");
//        logJson.put("request_time",sd.format(new Date()));
//        logJson.put("url",request.getRequestURI()==null?"":request.getRequestURL());
//        logJson.put("ip",request.getRemoteAddr());
//        logJson.put("signature",signature);
//        logJson.put("args",args);
//        logJson.put("time",time+" ms");
//        log.info("发送info日志:{}",logJson);
//        kafkaSender.send(infoTopic,logJson);
//
//    }
//
//    @Async("Log")
//    public void senErrorLog(HttpServletRequest request,Exception e)  {
//
//
//        Map<String,Object> errorLog = new HashMap();
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        errorLog.put("type","error");
//        errorLog.put("time",df.format(new Date()));
//        errorLog.put("message",e.getMessage());
//        errorLog.put("url",request.getRequestURL().toString());
//        errorLog.put("ip",request.getRemoteAddr());
//        errorLog.put("exception",e);
//        log.info("开始发送error日志{:}",errorLog);
//        kafkaSender.send(errorTopic,errorLog);
//
//
//
//    }
//
//    @Async
//    public Future<String> sayHello() throws InterruptedException {
//        String result="返回值";
//
//        return new AsyncResult<String>(result);
//    }
//
//
//}
