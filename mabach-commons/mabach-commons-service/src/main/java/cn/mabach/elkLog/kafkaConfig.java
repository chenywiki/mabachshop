//package cn.mabach.elkLog;
//
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.common.serialization.StringSerializer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.kafka.annotation.EnableKafka;
//import org.springframework.kafka.core.DefaultKafkaProducerFactory;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.core.ProducerFactory;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * @Author: ming
// * @Date: 2020/2/12 0012 上午 12:27
// */
//@Configuration
//@EnableKafka
//public class kafkaConfig {
////    @Value("${spring.kafka.bootstrap-servers}")
////    private String bootstrap_servers;
////    @Value("${spring.kafka.key-deserializer}")
////    private String key_deserializer;
////    @Value("${spring.kafka.value-deserializer}")
////    private String value_deserializer;
//
//    @Bean
//    public ProducerFactory<String, Object> producerFactory() {
//
//        Map<String, Object> props = new HashMap<>();
//        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.65.165:9092");
//        props.put(ProducerConfig.RETRIES_CONFIG, 1);
//        props.put(ProducerConfig.ACKS_CONFIG, "all");
//        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//
//        return new DefaultKafkaProducerFactory<>(props);
//    }
//
//    @Bean
//    @Primary
//    public KafkaTemplate<String,Object> getKafka(){
//        return new KafkaTemplate<String, Object>(producerFactory());
//    }
//}
