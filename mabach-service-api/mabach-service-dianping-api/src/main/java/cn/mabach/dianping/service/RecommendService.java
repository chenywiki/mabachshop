package cn.mabach.dianping.service;

import cn.mabach.dianping.entity.RecommendEntity;
import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;


/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-02-28 18:02:46
 */
public interface RecommendService {

    @GetMapping("/findRecommendList")
    @ApiOperation(value = "分页查询")
    public PageResult queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);

    @GetMapping("/infoRecommend")
    @ApiOperation(value = "根据id查找")
    RecommendEntity getByIdE(@RequestParam("id") Integer id);

    @PostMapping("/saveRecommend")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody RecommendEntity entity);

    @PostMapping("/updateRecommend")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody RecommendEntity entity);

    @PostMapping("/deleteOneRecommend")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Integer id);

    @PostMapping("/deleteRecommends")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Integer> ids);
}

