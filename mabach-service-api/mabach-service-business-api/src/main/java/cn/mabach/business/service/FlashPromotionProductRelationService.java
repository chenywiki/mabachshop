package cn.mabach.business.service;


import cn.mabach.business.dto.FlashPromotionProduct;
import cn.mabach.business.entity.FlashPromotionProductRelationEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 商品限时购与商品关系表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@Api(tags = "FlashPromotionProductRelationService", description = "限时购和商品关系管理")
public interface FlashPromotionProductRelationService   {

    @GetMapping("/listFlashPromotionProductRelation")
    @ApiOperation("分页查询不同场次关联及商品信息")
    RS<PageResult<FlashPromotionProduct>> queryPage(@RequestParam(value = "flashPromotionId",required = false) Long flashPromotionId,
                                                    @RequestParam(value = "flashPromotionSessionId",required = false) Long flashPromotionSessionId,
                                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoFlashPromotionProductRelation")
    @ApiOperation(value = "根据id查找")
    RS<FlashPromotionProductRelationEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveFlashPromotionProductRelation")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody FlashPromotionProductRelationEntity entity);

    @PostMapping("/saveFlashPromotionProductRelations")
    @ApiOperation(value = "批量保存")
    RS saveS(@RequestBody List<FlashPromotionProductRelationEntity> list);

    @PostMapping("/updateFlashPromotionProductRelation")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody FlashPromotionProductRelationEntity entity);

    @PostMapping("/deleteFlashPromotionProductRelations")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteFlashPromotionProductRelationByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;


    @GetMapping(value = "/getCountByPidAndPro")
    @ApiOperation("根据活动和场次id获取商品关系数量")
    long getCount(@RequestParam("flashPromotionId") Long flashPromotionId,@RequestParam("flashPromotionSessionId")Long flashPromotionSessionId);
}

