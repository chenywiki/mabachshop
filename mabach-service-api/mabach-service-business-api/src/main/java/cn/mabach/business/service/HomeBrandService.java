package cn.mabach.business.service;


import cn.mabach.business.entity.HomeBrandEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 首页推荐品牌表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@Api(tags = "HomeBrandService", description = "首页品牌管理")
public interface HomeBrandService   {

    @GetMapping("/listHomeBrand")
    @ApiOperation(value = "分页查询")
    RS<PageResult<HomeBrandEntity>> queryPage(@RequestParam(value = "brandName", required = false) String brandName,
                                              @RequestParam(value = "recommendStatus", required = false) Integer recommendStatus,
                                              @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                              @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoHomeBrand")
    @ApiOperation(value = "根据id查找")
    RS<HomeBrandEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveHomeBrand")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody HomeBrandEntity entity);

    @PostMapping("/updateHomeBrand")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody HomeBrandEntity entity);

    @PostMapping("/deleteHomeBrands")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteHomeBrandById", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @ApiOperation("修改品牌排序")
    @RequestMapping(value = "/updateBrandSortOne", method = RequestMethod.POST)
    public RS updateSort(@RequestParam("id") Long id, @RequestParam("sort") Integer sort);

    @ApiOperation("批量修改推荐状态")
    @RequestMapping(value = "/updateHomeBrandRecommendStatus", method = RequestMethod.POST)
    public RS updateRecommendStatus(@RequestParam("ids") List<Long> ids, @RequestParam Integer recommendStatus);
}

