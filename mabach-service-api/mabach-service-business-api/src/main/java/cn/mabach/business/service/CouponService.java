package cn.mabach.business.service;


import cn.mabach.business.entity.CouponEntity;
import cn.mabach.business.dto.CouponParam;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 优惠卷表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 17:40:52
 */
@Api(tags = "CouponService", description = "优惠券管理")
public interface CouponService   {

    @GetMapping("/listCoupon")
    @ApiOperation("根据优惠券名称和类型分页获取优惠券列表")
    RS<PageResult<CouponEntity>> queryPage(@RequestParam(value = "name",required = false) String name,
                                           @RequestParam(value = "type",required = false) Integer type,
                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoCoupon")
    @ApiOperation(value = "根据id查找")
    RS<CouponParam> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveCoupon")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody CouponParam entity);

    @PostMapping("/updateCoupon")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody CouponParam entity);

    @PostMapping("/deleteCoupons")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("删除优惠券")
    @RequestMapping(value = "/deleteCoupon", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

}

