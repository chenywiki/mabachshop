package cn.mabach.business.service;


import cn.mabach.business.entity.CouponHistoryEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;
/**
 * 优惠券使用、领取历史表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 17:40:52
 */
@Api(tags = "CouponHistoryService", description = "优惠券领取记录管理")
public interface CouponHistoryService   {

    @GetMapping("/listCouponHistory")
    @ApiOperation("根据优惠券id，使用状态，订单编号分页获取领取记录")
    RS<PageResult<CouponHistoryEntity>> queryPage(@RequestParam(value = "couponId", required = false) Long couponId,
                                                  @RequestParam(value = "useStatus", required = false) Integer useStatus,
                                                  @RequestParam(value = "orderSn", required = false) String orderSn,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoCouponHistory")
    @ApiOperation(value = "根据id查找")
    RS<CouponHistoryEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveCouponHistory")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody CouponHistoryEntity entity);

    @PostMapping("/updateCouponHistory")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody CouponHistoryEntity entity);

    @PostMapping("/deleteCouponHistory")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);
}

