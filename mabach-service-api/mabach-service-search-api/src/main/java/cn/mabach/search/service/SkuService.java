package cn.mabach.search.service;


import cn.mabach.result.TableDataInfo;
import cn.mabach.result.RS;
import cn.mabach.search.entity.SkuEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 商品表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-02-17 17:59:18
 */
@Api(tags = "广告服务接口")
public interface SkuService   {

    @GetMapping("/listSku")
    @ApiOperation(value = "分页查询")
    TableDataInfo queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoSku")
    @ApiOperation(value = "根据id查找")
    RS<SkuEntity> getByIdE(@RequestParam("id") String id);

    @GetMapping("/infoSkuAll")
    @ApiOperation(value = "查询所有")
    RS<List<SkuEntity>> getall();


    @PostMapping("/saveSku")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody SkuEntity entity);


    @PutMapping("/updateSku")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody SkuEntity entity);

    @DeleteMapping("/deleteSkus")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);


}

