package cn.mabach.member.service;


import cn.mabach.member.entity.MemberLoginLogEntity;
import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 会员登录记录
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-04 17:30:47
 */
@Api(tags = "会员日志")
public interface MemberLoginLogService   {

    @GetMapping("/listMemberLoginLog")
    @ApiOperation(value = "分页查询")
    TableDataInfo queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoMemberLoginLog")
    @ApiOperation(value = "根据id查找")
    RS<MemberLoginLogEntity> getByIdE(@RequestParam("id") Long id);


    @PostMapping("/saveMemberLoginLog")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody MemberLoginLogEntity entity);


    @PutMapping("/updateMemberLoginLog")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody MemberLoginLogEntity entity);

    @DeleteMapping("/deleteMemberLoginLogs")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);


}

