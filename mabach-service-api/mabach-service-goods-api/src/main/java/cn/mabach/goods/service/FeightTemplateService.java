package cn.mabach.goods.service;

import cn.mabach.goods.entity.FeightTemplateEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 运费模版
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 01:04:21
 */
@Api(tags = "广告服务接口")
public interface FeightTemplateService   {

    @GetMapping("/listFeightTemplate")
    @ApiOperation(value = "分页查询")
    RS<PageResult<FeightTemplateEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);

    @GetMapping("/infoFeightTemplate")
    @ApiOperation(value = "根据id查找")
    RS<FeightTemplateEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveFeightTemplate")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody FeightTemplateEntity entity);

    @PostMapping("/updateFeightTemplate")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody FeightTemplateEntity entity);

    @PostMapping("/deleteOneFeightTemplate")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Long id);

    @PostMapping("/deleteFeightTemplate")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Long> ids);
}

