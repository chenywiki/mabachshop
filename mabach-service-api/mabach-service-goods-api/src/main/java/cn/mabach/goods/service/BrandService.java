package cn.mabach.goods.service;


import cn.mabach.goods.entity.BrandEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 品牌表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:24:35
 */
@Api(tags = "PmsBrandController", description = "商品品牌管理")
public interface BrandService   {

    @GetMapping("/listBrand")
    @ApiOperation(value = "根据品牌名称分页查询")
    RS<PageResult<BrandEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize);


    @GetMapping("/listAllBrand")
    @ApiOperation(value = "获取全部品牌列表")
    RS<List<BrandEntity>> queryAll();

    @GetMapping("/infoBrand")
    @ApiOperation(value = "根据id查找品牌信息")
    RS<BrandEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveBrand")
    @ApiOperation(value = "添加品牌")
    RS saveE(@RequestBody BrandEntity entity);

    @PostMapping("/updateBrand")
    @ApiOperation(value = "更新品牌")
    RS updateByIdE(@RequestBody BrandEntity entity);

    @GetMapping("/deleteOneBrand")
    @ApiOperation(value = "根据ID删除品牌")
    RS removeOneById(@RequestParam("id") Long id);

    @PostMapping("/deleteBrand")
    @ApiOperation(value = "批量删除品牌")
    RS removeByIdsE(@RequestBody List<Long> ids);

    @ApiOperation(value = "批量更新显示状态")
    @PostMapping(value = "/update/showStatusBrand")
    public RS updateShowStatus(@RequestParam("ids") List<Long> ids,
                               @RequestParam("showStatus") Integer showStatus);

    @ApiOperation(value = "批量更新厂家制造商状态")
    @PostMapping(value = "/update/factoryStatusBrand")
    public RS updateFactoryStatus(@RequestParam("ids") List<Long> ids,
                                  @RequestParam("factoryStatus") Integer factoryStatus);
}

