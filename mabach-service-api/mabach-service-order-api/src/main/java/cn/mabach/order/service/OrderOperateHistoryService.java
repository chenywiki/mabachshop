package cn.mabach.order.service;


import cn.mabach.order.entity.OrderOperateHistoryEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 订单操作历史记录
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@FeignClient(name = "app-mabach-order")
@Api(tags = "广告服务接口")
public interface OrderOperateHistoryService   {

    @GetMapping("/listOrderOperateHistory")
    @ApiOperation(value = "分页查询")
    RS<PageResult<OrderOperateHistoryEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoOrderOperateHistory")
    @ApiOperation(value = "根据id查找")
    RS<OrderOperateHistoryEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveOrderOperateHistory")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody OrderOperateHistoryEntity entity);

    @PostMapping("/updateOrderOperateHistory")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody OrderOperateHistoryEntity entity);

    @PostMapping("/deleteOrderOperateHistorys")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteOrderOperateHistoryByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @GetMapping("/getOrderOperateHistoryEntityByOrderId")
    @ApiOperation(value = "根据订单id查找")
    List<OrderOperateHistoryEntity> getOrderOperateHistoryEntityByOrderId(@RequestParam("id") Long id);
}

