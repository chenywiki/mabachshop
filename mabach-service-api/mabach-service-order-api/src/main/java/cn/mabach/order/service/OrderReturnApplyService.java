package cn.mabach.order.service;


import cn.mabach.order.dto.OrderReturnApplyResult;
import cn.mabach.order.entity.OrderReturnApplyEntity;
import cn.mabach.order.vo.UpdateStatusParam;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
/**
 * 订单退货申请
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@FeignClient(name = "app-mabach-order")
@Api(tags = "OrderReturnApplyService", description = "订单退货申请管理")
public interface OrderReturnApplyService   {

    @GetMapping("/listOrderReturnApply")
    @ApiOperation(value = "分页查询")
    RS<PageResult<OrderReturnApplyEntity>> queryPage(@RequestParam(value = "id",required = false) Long id,
                                                     @RequestParam(value = "receiverKeyword",required = false) String receiverKeyword,
                                                     @RequestParam(value = "status",required = false) Integer status,
                                                     @RequestParam(value = "createTime",required = false) Date createTime,
                                                     @RequestParam(value = "handleMan",required = false) String handleMan,
                                                     @RequestParam(value = "handleTime",required = false) Date handleTime,
                                                     @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                     @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoOrderReturnApply")
    @ApiOperation(value = "根据id查找")
    RS<OrderReturnApplyResult> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveOrderReturnApply")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody OrderReturnApplyEntity entity);

    @PostMapping("/updateOrderReturnApply")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody OrderReturnApplyEntity entity);

    @PostMapping("/deleteOrderReturnApplys")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteOrderReturnApplyByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @ApiOperation("修改申请状态")
    @RequestMapping(value = "/updateUpdateStatusParamStatus", method = RequestMethod.POST)
    public RS updateStatus(@RequestBody UpdateStatusParam statusParam);
}

