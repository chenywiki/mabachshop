package cn.mabach.order.service;


import cn.mabach.order.entity.CompanyAddressEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 公司收发货地址表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@FeignClient(name = "app-mabach-order")
@Api(tags = "CompanyAddressService", description = "收货地址管理")
public interface CompanyAddressService   {

    @GetMapping("/listCompanyAddress")
    @ApiOperation(value = "分页查询")
    RS<PageResult<CompanyAddressEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoCompanyAddress")
    @ApiOperation(value = "根据id查找")
    RS<CompanyAddressEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveCompanyAddress")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody CompanyAddressEntity entity);

    @PostMapping("/updateCompanyAddress")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody CompanyAddressEntity entity);

    @PostMapping("/deleteCompanyAddresss")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteCompanyAddressByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;
}

