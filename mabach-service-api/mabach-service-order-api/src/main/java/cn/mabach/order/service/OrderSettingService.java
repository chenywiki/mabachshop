package cn.mabach.order.service;


import cn.mabach.order.entity.OrderSettingEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 订单设置表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@FeignClient(name = "app-mabach-order")
@Api(tags = "OrderSettingService", description = "订单设置管理")
public interface OrderSettingService   {

    @GetMapping("/listOrderSetting")
    @ApiOperation(value = "分页查询")
    RS<PageResult<OrderSettingEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoOrderSetting")
    @ApiOperation(value = "根据id查找")
    RS<OrderSettingEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveOrderSetting")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody OrderSettingEntity entity);

    @PostMapping("/updateOrderSetting")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody OrderSettingEntity entity);

    @PostMapping("/deleteOrderSettings")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteOrderSettingByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;
}

